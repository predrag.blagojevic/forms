/**
 * Generate CSS class list
 *
 * @param {Array} classes - Classes as arguments
 * @returns {String} Space separated list of classes
 */
export const classNames = (...classes) => classes.filter(Boolean).join(" ");

/**
 * Omit attributes from an object
 *
 * @param {Object} object - Input object
 * @param {Array} keys - List of keys to omit
 * @returns {Object} New object
 */
export const omit = (object, keys) =>
    Object.entries(object)
        .filter(([key]) => !keys.includes(key))
        .reduce(
            (acc, [key, value]) => Object.assign(acc, { [key]: value }),
            {},
        );

/**
 * Make a string in camelCase
 *
 * @param {String} text - String to camelize
 * @returns {String} Camelized string
 */
export const camelize = text =>
    text
        ? text.replace(/^([A-Z])|[\s-_]+(\w)/g, (match, p1, p2, offset) => {
              if (p2) {
                  return p2.toUpperCase();
              }

              return p1.toLowerCase();
          })
        : "";

/**
 * Check if an object is Promise
 *
 * @param {Object} object - Input object
 * @returns {Boolean} True if Promise
 */
export const isPromise = object =>
    Object.prototype.toString.call(object) === "[object Promise]";

/**
 * Check if a variable is a Function
 *
 * @param {any} variable - Input object
 * @returns {Boolean} True if Function
 */
export const isFunction = variable => variable instanceof Function;

/**
 * Returns a function that accepts data/aria specific attributes
 *
 * @param {String} type - Attribute type: "data", "aria", etc.
 * @returns {Object} Key value object
 */
export const createAttributes = type => data =>
    Object.keys(data).reduce(
        (acc, attr) => ({ ...acc, [`${type}-${attr}`]: data[attr] }),
        {},
    );

/**
 * Returns a unique identifier
 *
 * @returns {String} uuid
 */
export const uuid = () =>
    "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, c => {
        const r = (Math.random() * 16) | 0;
        const v = c === "x" ? r : (r & 0x3) | 0x8;
        return v.toString(16);
    });
