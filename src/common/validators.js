export const required = value => (value ? null : "required");

export const minlen = length => value =>
    value && value.length < length
        ? `minimum length is ${length} characters`
        : undefined;

export const maxlen = length => value =>
    value && value.length > length
        ? `maximum length is ${length} characters`
        : undefined;

export const number = value =>
    value === undefined || value === null
        ? undefined
        : value && isNaN(value)
        ? "value must be number"
        : undefined;

export const integer = value =>
    Number.isInteger(Number(value)) ? undefined : "value should be integer";

export const string = value =>
    value && /[^a-zA-Z ]/i.test(value) ? "value must be string" : undefined;

export const alphanumeric = value =>
    value && /[^a-zA-Z0-9 ]/i.test(value)
        ? "value must be alphanumeric"
        : undefined;

export const phone = value =>
    value && !/^(0|[1-9][0-9]{9})$/i.test(value)
        ? "invalid phone format"
        : undefined;

export const email = value =>
    value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
        ? "invalid email format"
        : undefined;

export const password = value =>
    value && value.indexOf(" ") > -1
        ? "spaces not allowed"
        : value && value.length < 8
        ? "minimum length is 8"
        : undefined;

export const match = toValue => value =>
    value !== toValue ? "values don't match" : undefined;

export const min = minVal => value =>
    value && value < minVal ? `minimum value is ${minVal}` : undefined;

export const max = maxVal => value =>
    value && value > maxVal ? `maximum value is ${maxVal}` : undefined;
