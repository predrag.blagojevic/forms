import React from "react";
import Form, { Field } from "$components/Form";
import { required, email, password } from "$common/validators";

const layoutStyle = {
    display: "grid",
    gridTemplateColumns: "min-content min-content",
    gridGap: "1rem",
};

const LoginForm = () => {
    const handleSubmit = data => {
        console.log("LoginForm.handleSubmit()", data);
        return new Promise((resolve, reject) =>
            setTimeout(() => resolve("Form submitted!", data), 1000),
        );
    };

    const handleValidation = (isValid, validation) => {
        console.log("LoginForm.handleValidation()", isValid, validation);
    };

    return (
        <section style={layoutStyle}>
            <Form
                title="Login Form"
                submit="Login"
                onSubmit={handleSubmit}
                onValidate={handleValidation}
            >
                <Field
                    type="email"
                    name="email"
                    label="Email"
                    required
                    validators={[required, email]}
                />
                <Field
                    type="password"
                    name="password"
                    label="Password"
                    required
                    validators={[required, password]}
                />
                <Field
                    type="checkbox"
                    name="remember"
                    label="Remember me"
                    value="yes"
                />
            </Form>

            <Form
                title="Sign in Form"
                header={<small>(horizontal)</small>}
                submit="Sign In"
                layout="horizontal"
                onSubmit={handleSubmit}
                onValidate={handleValidation}
            >
                <Field
                    type="email"
                    name="email"
                    label="Email"
                    required
                    validators={[required, email]}
                />
                <Field
                    type="password"
                    name="password"
                    label="Password"
                    required
                    validators={[required, password]}
                />
                <Field
                    type="checkbox"
                    name="remember"
                    label="Remember me"
                    value="yes"
                />
            </Form>
        </section>
    );
};

export default LoginForm;
