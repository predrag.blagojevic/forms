import React from "react";
import Form, { Row, Field } from "$components/Form";
import {
    required,
    minlen,
    integer,
    password,
    maxlen,
} from "$common/validators";

const DemoForm = () => {
    const handleSubmit = data => {
        console.log("DemoForm.handleSubmit()", data);
        return new Promise((resolve, reject) =>
            setTimeout(() => reject("Error submitting the form.", data), 1000),
        );
    };

    const handleValidation = (isValid, validation) => {
        console.log("DemoForm.handleValidation()", isValid, validation);
    };

    return (
        <div>
            <Form
                title="Demo Form 1"
                submit="Submit"
                reset="Reset"
                layout="horizontal"
                width="full"
                onSubmit={handleSubmit}
                onValidate={handleValidation}
            >
                <Row>
                    <Field
                        type="text"
                        name="firstName"
                        label="First name"
                        required
                        validators={[required, minlen(2)]}
                    />
                    <Field
                        type="text"
                        name="lastName"
                        placeholder="Last name"
                        required
                        validators={[required]}
                    />
                </Row>
                <Field
                    type="password"
                    name="password"
                    label="Password"
                    hint="min. 8 characters"
                    validators={[required, password]}
                />
                <Field
                    type="number"
                    name="age"
                    label="Age"
                    width="small"
                    validators={[integer]}
                />
                <Field
                    type="text"
                    hint="Hint"
                    label="Label"
                    placeholder="Placeholder"
                    value="Text"
                    messages={["message 1"]}
                    status="error"
                    width="large"
                    required
                    validators={[required]}
                />
                <Field
                    type="checkbox"
                    name="accept"
                    label="Accept"
                    hint="Please accept"
                    value="yes"
                    required
                    // checked
                    indeterminate
                    validators={[required]}
                />
                <Field
                    type="textarea"
                    name="comment"
                    label="Comment"
                    width="full"
                    validators={[maxlen(100)]}
                />
                <Field
                    type="radio"
                    name="radioGroup1"
                    label="Radio group"
                    selected="on"
                    multiple={[
                        { value: "on", label: "Selected" },
                        { value: "off", label: "Inactive" },
                        {
                            value: "idle",
                            label: "Disabled",
                            hint: "(disabled radio button)",
                            disabled: true,
                        },
                    ]}
                    inline={false}
                />
                <Field
                    type="radio"
                    name="radioGroup2"
                    selected="two"
                    multiple={[
                        { value: "one", label: "One" },
                        { value: "two", label: "Two" },
                        { value: "three", label: "Three" },
                    ]}
                    hint="Hint message..."
                />
            </Form>
            <hr />
            <Form
                width="full"
                title="Demo 2"
                header={<small>Subtitle</small>}
                submit="Submit"
                reset="Reset"
                footer={
                    <a href="#" style={{ marginLeft: "1em" }}>
                        Action
                    </a>
                }
                onSubmit={handleSubmit}
                onValidate={handleValidation}
            >
                <Field
                    type="textarea"
                    name="comment1"
                    label="Comment"
                    width="full"
                    validators={[maxlen(100)]}
                />
                <Field
                    type="radio"
                    name="radioGroup1"
                    label="On / off"
                    selected="on"
                    multiple={[
                        { value: "on", label: "Selected" },
                        { value: "off", label: "Inactive" },
                        {
                            value: "idle",
                            label: "Disabled",
                            hint: "(disabled radio button)",
                            disabled: true,
                        },
                    ]}
                    inline={false}
                />
                <Field
                    type="radio"
                    name="radioGroup2"
                    selected="two"
                    multiple={[
                        { value: "one", label: "One" },
                        { value: "two", label: "Two" },
                        { value: "three", label: "Three" },
                    ]}
                    hint="Hint message..."
                />
                <Field
                    type="select"
                    name="select1"
                    label="Select"
                    options={[
                        { value: "", text: "Select..." },
                        { value: "key1", text: "Option 1" },
                        { value: "key2", text: "Option 2" },
                        { value: "key3", text: "Option 3" },
                    ]}
                    required
                    validators={[required]}
                />
                <Field
                    type="select"
                    name="select2"
                    label="Multiline select"
                    width="fixed"
                    options={[
                        { value: "key1", text: "Option 1" },
                        { value: "key2", text: "Selected" },
                        { value: "key3", text: "Option 3" },
                        { value: "key4", text: "Option 4" },
                    ]}
                    selected="key2"
                    multiple
                />
                <Field
                    type="switch"
                    name="onOff1"
                    label="Switch 1"
                    value="on"
                />
                <Field
                    type="switch"
                    name="onOff2"
                    label="Switch 2"
                    value="on"
                />
                <Field
                    type="switch"
                    name="onOff3"
                    label="Switch 3"
                    value="on"
                    checked
                />
                <Field
                    type="switch"
                    name="onOff4"
                    label="Switch 4"
                    value="on"
                    checked
                />
                <Field
                    type="switch"
                    name="onOff5"
                    label="Switch 5"
                    value="on"
                />
            </Form>
        </div>
    );
};

export default DemoForm;
