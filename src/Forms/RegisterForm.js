import React, { useState } from "react";
import Form, { Row, Field } from "$components/Form";
import { required, string, email, password, match } from "$common/validators";

const layoutStyle = {
    display: "grid",
    gridTemplateColumns: "min-content min-content",
    gridGap: "1rem",
};

const RegisterForm = () => {
    const [state, setState] = useState({
        password: "",
        repeat: "",
    });

    const handleSubmit = data => {
        console.log("RegisterForm.handleSubmit()", data);
        return new Promise((resolve, reject) =>
            setTimeout(() => resolve("Form submitted!", data), 1000),
        );
    };

    const handleValidation = (isValid, validation) => {
        console.log("RegisterForm.handleValidation()", isValid, validation);
    };

    return (
        <section style={layoutStyle}>
            <Form
                title="Register"
                submit="Submit"
                onSubmit={handleSubmit}
                onValidate={handleValidation}
            >
                <Field
                    type="text"
                    name="firstName"
                    label="First Name"
                    width="large"
                    required
                    validators={[required, string]}
                />
                <Field
                    type="text"
                    name="lastName"
                    label="Last Name"
                    width="large"
                    required
                    validators={[required, string]}
                />
                <Field
                    type="email"
                    name="email"
                    label="Email"
                    width="full"
                    required
                    validators={[required, email]}
                />
                <Row>
                    <Field
                        type="password"
                        name="password"
                        label="Password"
                        hint="min. 8 characters"
                        required
                        validators={[required, password]}
                        onChange={value =>
                            setState(state => ({ ...state, password: value }))
                        }
                    />
                    <Field
                        type="password"
                        name="repeat"
                        placeholder="Repeat your password"
                        required
                        validators={[match(state.password)]}
                        onChange={value =>
                            setState(state => ({ ...state, repeat: value }))
                        }
                    />
                </Row>

                <Field
                    type="checkbox"
                    name="remember"
                    label="Accept"
                    hint="By ticking this box you agree to accept our Terms of Service and Privacy Policy."
                    required
                    value="yes"
                    validators={[required]}
                />
            </Form>
        </section>
    );
};

export default RegisterForm;
