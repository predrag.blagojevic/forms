import React from "react";
import Form, { Row, Field } from "$components/Form";
import { required, string, integer, maxlen } from "$common/validators";

const UserContactForm = () => {
    const handleSubmit = data => {
        console.log("UserContactForm.handleSubmit()", data);
        return new Promise((resolve, reject) =>
            setTimeout(
                () =>
                    resolve(
                        "Thank you for your message. We will get in touch with you soon!",
                        data,
                    ),
                1000,
            ),
        );
    };

    const handleValidation = (isValid, validation) => {
        console.log("UserContactForm.handleValidation()", isValid, validation);
    };

    return (
        <Form
            title="Contact Form"
            submit="Submit"
            reset="Reset"
            width="full"
            onSubmit={handleSubmit}
            onValidate={handleValidation}
        >
            <Field
                type="text"
                name="firstName"
                label="First name"
                required
                focus
                validators={[required, string]}
            />
            <Field
                type="text"
                name="lastName"
                label="Last name"
                width="large"
                validators={[string]}
            />
            <Row>
                <Field
                    type="number"
                    name="area"
                    label="Area Code"
                    width="small"
                    validators={[integer]}
                />
                <Field
                    type="number"
                    name="phone"
                    label="Phone number"
                    width="small"
                    required
                    validators={[required, integer, maxlen(7)]}
                />
            </Row>
        </Form>
    );
};

export default UserContactForm;
