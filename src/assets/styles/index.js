import React from "react";
import { Global, css } from "@emotion/core";

import variables from "./variables.js";
import normalize from "./normalize.js";
import layout from "./layout.js";
import loader from "$components/Loader/styles.js";
import button from "$components/Button/styles.js";
import label from "$components/Label/styles.js";
import input from "$components/Input/styles.js";
import checkbox from "$components/Checkbox/styles.js";
import textarea from "$components/Textarea/styles.js";
import radio from "$components/Radio/styles.js";
import select from "$components/Select/styles.js";
import form from "$components/Form/styles.js";

const styles = css`
    ${variables}
    ${normalize}
    ${layout}
    ${loader}
    ${button}
    ${label}
    ${input}
    ${checkbox}
    ${textarea}
    ${radio}
    ${select}
    ${form}
`;

export default () => <Global {...styles} />;
