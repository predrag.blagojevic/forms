export default `
:root {
    --vr: 24px;
    --line-height: 24px;
    --font-size: 16px;
    --font-size-xxs: 10px;
    --font-size-xs: 12px;
    --font-size-s: 14px;
    --font-size-m: 18px;
    --font-size-l: 20px;
    --font-size-xl: 24px;
    --font-size-xxl: 36px;
    --border-size: 1px;
    --color-primary: #0fa980;
    --color-secondary: #51616d;
    --color-window: #fff;
    --color-text: #303438;
    --color-link: #0aa7b9;
    --color-hover: #0b8363;
    --color-border: #ced4da;
    --color-separator: #eee;
    --color-control: #fff;
    --color-error: #ee6160;
    --color-hint: #8393a0;
}
`;
