export default `
    :root {
        line-height: var(--vr);
        font-size: var(--vr);
    }

    body {
        padding: var(--vr);
        font-size: var(--font-size);
        color: var(--color-text);
        background: var(--color-window);
    }

    :focus {
        outline: none;
        text-decoration: none;
    }

    [disabled],
    [aria-disabled="true"] {
        opacity: 0.5;
        cursor: not-allowed;
        pointer-events: none;
    }

    [hidden],
    [aria-hidden="true"] {
        display: none !important;
    }

    h1 {
        margin: 1rem 0;
        font-size: var(--font-size-xxl);
        line-height: 2rem;
    }

    h2 {
        margin: 0.75rem 0;
        font-size: var(--font-size-xl);
        line-height: 1.5rem;
    }

    h3 {
        margin: 1rem 0;
        font-size: var(--font-size-l);
        line-height: 1rem;
    }

    h4 {
        margin: 1rem 0;
        font-size: var(--font-size-m);
        line-height: 1rem;
    }

    h5 {
        margin: 1rem 0;
        font-size: var(--font-size);
        line-height: 1rem;
    }

    h6 {
        margin: 1rem 0;
        font-size: var(--font-size-s);
        line-height: 1rem;
    }

    small {
        font-size: var(--font-size-s);
    }

    a {
        color: var(--color-link);
        text-decoration: none;

        &:hover {
            color: var(--color-hover);
        }
    }

    fieldset {
        border: 0;
        padding: .01em 0 0 0;
        margin: 0;
        min-width: 0;
    }

    legend {
        padding: 0;
        display: table;
    }

    hr {
        width: 100%;
        height: 0;
        margin: 1rem 0;
        border: 0;
        border-top: 1px solid var(--color-separator);
    }

    ::-webkit-scrollbar {
        width: 9px;
    }

    ::-webkit-scrollbar-track {
        border-radius: 5px;
        background: rgba(0, 0, 0, .05);
    }

    ::-webkit-scrollbar-thumb {
        border-radius: 5px;
        background: rgba(0, 0, 0, .05);

        &:window-inactive {
            background: rgba(0, 0, 0, .05);
        }

        &:hover {
            background: rgba(0, 0, 0, .1);
        }
      }
`;
