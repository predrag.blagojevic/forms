export default `
.amb-textarea {
    --textarea-width: 375px;
    --textarea-height: 4.5rem;

    appearance: none;
    display: inline-block;
    width: var(--textarea-width);
    height: var(--textarea-height);
    margin: 0.25rem 0;
    padding: 0.25rem 1em;
    line-height: 1rem;
    vertical-align: top;
    // resize: none;
    overflow: hidden;

    color: var(--color-secondary);
    background: var(--color-control);
    box-shadow: none;
    border: 1px solid var(--color-border);
    border-radius: 5px;
    transition: border-color .15s ease-in-out, box-shadow .15s ease-in-out;

    &:focus {
        outline: none;
        z-index: 1;
        transform: scale(1,1);
        border-color: var(--color-hover);
    }

    &:hover {
        &:not([disabled]),
        &:not([aria-disabled='true']) {
            border-color: var(--color-primary);
        }
    }

    &::placeholder {
      color: var(--color-border);
      font-size: var(--font-size-s);
      font-style: normal;
    }

    & + .amb-label {
        margin-left: 1ex;
    }

    &:-webkit-autofill,
    &:-webkit-autofill:hover,
    &:-webkit-autofill:focus {
        border-color: var(--color-border) !important;
        -webkit-text-fill-color: var(--color-secondary) !important;
        -webkit-box-shadow: 0 0 0 1000px var(--color-control) inset !important;
    }

    &--full {
        width: 100%;
    }

    &--auto {
        width: auto;
    }

    &--error,
    &--error:focus,
    &--error:hover {
        &:not([disabled]),
        &:not([aria-disabled='true']) {
            border-color: var(--color-error);
        }
    }
}
`;
