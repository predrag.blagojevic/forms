import React, {
    useState,
    useEffect,
    useRef,
    forwardRef,
    useImperativeHandle,
} from "react";
import { classNames } from "$common/util";

const baseClass = "amb-textarea";

const initialState = {
    value: "",
    runValidation: true,
};

const defaultProps = {
    type: "text",
    value: "",
    onChange: () => {},
    onKeyDown: () => {},
    onBlur: () => {},
    onValidate: () => {},
};

const Textarea = forwardRef((props, ref) => {
    const [state, setState] = useState(initialState);
    const elRef = useRef();
    const isFirstRun = useRef(true); // used to distinguish the first pass

    const {
        id,
        name,
        value,
        defaultValue,
        validators,
        placeholder,
        title,
        focus,
        tabIndex,
        minLength,
        maxLength,
        disabled,
        readOnly,
        hidden,
        required,
        autoFocus,
        status,
        width,
        size,
        className,
        onBlur,
        onDragStart,
        onDrop,
        onFocus,
        onChange,
        onValidate,
    } = props;

    const classes = classNames(
        baseClass,
        width && `${baseClass}--${width}`,
        size && `${baseClass}--${size}`,
        status && `${baseClass}--${status}`,
        className,
    );

    // make validate() function accessible via reference
    useImperativeHandle(ref, () => ({ validate, reset }));

    // didMount
    useEffect(() => focus && elRef.current.focus(), []);

    // set state.value when props.value changes
    useEffect(() => setState(state => ({ ...state, value })), [props.value]);

    // validate on state.value change
    useEffect(() => {}, [state.value]);
    useEffect(() => {
        if (isFirstRun.current) {
            isFirstRun.current = false;
            return;
        }

        if (state.runValidation) {
            validate();
        }

        setState(state => ({ ...state, runValidation: true }));
    }, [state.value]);

    /**
     * Check field validity with provided validators
     *
     * @returns {Boolean} True if all the conditions are satisfied
     */
    const validate = () => {
        // the rest is relevant only if validators are passed to a field
        if (!validators) {
            return true;
        }

        // check field validity by running validators
        const messages = validators
            .reduce((acc, validator) => [...acc, validator(state.value)], [])
            .filter(Boolean);

        const isValid = messages.length === 0;

        // notify parent Form about field's validity
        name &&
            onValidate({
                [name]: {
                    isValid,
                    messages,
                    value: state.value,
                },
            });

        return isValid;
    };

    /**
     * Reset field value
     *
     * @returns {Boolean} True if all the conditions are satisfied
     */
    const reset = () => setState({ ...initialState, runValidation: false });

    /**
     * Handle onChange event
     *
     * @param {Object} e - Input event
     * @returns {void}
     */
    const handleChange = ({ target: { value } }) => {
        setState(state => ({ ...state, value }));
        onChange(value);
    };

    /**
     * Handle onBlur event
     *
     * @param {Object} e - Input event
     * @returns {void}
     */
    const handleBlur = ({ target: { value } }) => {
        validate();
        onBlur(value);
    };

    return (
        <textarea
            id={id}
            name={name}
            value={state.value}
            defaultValue={defaultValue}
            placeholder={placeholder}
            title={title}
            tabIndex={tabIndex}
            minLength={minLength}
            maxLength={maxLength}
            disabled={disabled}
            readOnly={readOnly}
            hidden={hidden}
            required={required}
            autoFocus={autoFocus}
            ref={elRef}
            className={classes}
            onChange={handleChange}
            onBlur={handleBlur}
            onDragStart={onDragStart}
            onDrop={onDrop}
            onFocus={onFocus}
        />
    );
});

Textarea.defaultProps = defaultProps;

export default Textarea;
