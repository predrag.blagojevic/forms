import React, {
    useState,
    useEffect,
    useRef,
    forwardRef,
    useImperativeHandle,
} from "react";
import { classNames } from "$common/util";

const baseClass = "amb-input";

const initialState = {
    value: "",
    runValidation: true,
};

const defaultProps = {
    type: "text",
    value: "",
    onChange: () => {},
    onKeyDown: () => {},
    onBlur: () => {},
    onValidate: () => {},
};

const Input = forwardRef((props, ref) => {
    const [state, setState] = useState(initialState);
    const elRef = useRef();
    const isFirstRun = useRef(true); // used to distinguish the first pass

    const {
        type,
        id,
        name,
        value,
        defaultValue,
        validators,
        placeholder,
        title,
        focus,
        pattern,
        tabIndex,
        minLength,
        maxLength,
        min,
        max,
        disabled,
        readOnly,
        hidden,
        required,
        autoFocus,
        status,
        size,
        width,
        className,
        onBlur,
        onDragStart,
        onDrop,
        onFocus,
        onChange,
        onKeyDown,
        onValidate,
    } = props;

    const classes = classNames(
        baseClass,
        width && `${baseClass}--${width}`,
        size && `${baseClass}--${size}`,
        status && `${baseClass}--${status}`,
        className,
    );

    // make validate() function accessible via reference
    useImperativeHandle(ref, () => ({ validate, reset }));

    // didMount
    useEffect(() => focus && elRef.current.focus(), []);

    // set state.value when props.value changes
    useEffect(() => setState(state => ({ ...state, value })), [props.value]);

    // validate on state.value change
    useEffect(() => {}, [state.value]);
    useEffect(() => {
        if (isFirstRun.current) {
            isFirstRun.current = false;
            return;
        }

        if (state.runValidation) {
            validate();
        }

        setState(state => ({ ...state, runValidation: true }));
    }, [state.value]);

    /**
     * Check field validity with provided validators
     *
     * @returns {Boolean} True if all the conditions are satisfied
     */
    const validate = () => {
        // the rest is relevant only if validators are passed to a field
        if (!validators) {
            return true;
        }

        // check field validity by running validators
        const messages = validators
            .reduce((acc, validator) => [...acc, validator(state.value)], [])
            .filter(Boolean);

        const isValid = messages.length === 0;

        // notify parent Form about field's validity
        name &&
            onValidate({
                [name]: {
                    isValid,
                    messages,
                    value: state.value,
                },
            });

        return isValid;
    };

    /**
     * Reset field value
     *
     * @returns {Boolean} True if all the conditions are satisfied
     */
    const reset = () => setState({ ...initialState, runValidation: false });

    /**
     * Handle keyDown event
     *
     * @param {Object} e - Keyboard event
     * @returns {void}
     */
    const handleKeyDown = e => {
        // For number input type, allow only numbers and common special characters (and combinations)
        if (type === "number") {
            const { keyCode, ctrlKey, shiftKey, metaKey } = e;
            const clipboard = [65, 67, 86]; // a, c, v
            const specials = [
                46, // delete
                8, // backspace
                9, // tab
                27, // esc
                13, // enter
                110, // decimal point
                189, // minus
                190, // period
            ];

            if (
                specials.indexOf(keyCode) > -1 ||
                ((ctrlKey || metaKey) && clipboard.indexOf(keyCode) > -1) ||
                (keyCode >= 35 && keyCode <= 40) // end, home, left, up, right, down
            ) {
                return; // let it happen
            }

            if (
                (shiftKey || keyCode < 48 || keyCode > 57) &&
                (keyCode < 96 || keyCode > 105)
            ) {
                e.preventDefault(); // shift, not (numpad) numbers
            }
        }

        onKeyDown(e);
    };

    /**
     * Handle onChange event
     *
     * @param {Object} e - Input event
     * @returns {void}
     */
    const handleChange = ({ target: { value } }) => {
        setState(state => ({ ...state, value }));
        onChange(value);
    };

    /**
     * Handle onBlur event
     *
     * @param {Object} e - Input event
     * @returns {void}
     */
    const handleBlur = ({ target: { value } }) => {
        validate();
        onBlur(value);
    };

    return (
        <input
            type={type}
            id={id}
            name={name}
            value={state.value}
            defaultValue={defaultValue}
            placeholder={placeholder}
            title={title}
            pattern={pattern}
            tabIndex={tabIndex}
            minLength={minLength}
            maxLength={maxLength}
            min={min}
            max={max}
            disabled={disabled}
            readOnly={readOnly}
            hidden={hidden}
            required={required}
            autoFocus={autoFocus}
            ref={elRef}
            className={classes}
            onBlur={handleBlur}
            onFocus={onFocus}
            onDragStart={onDragStart}
            onDrop={onDrop}
            onChange={handleChange}
            onKeyDown={handleKeyDown}
        />
    );
});

Input.defaultProps = defaultProps;

export default Input;
