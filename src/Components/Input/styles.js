export default `
.amb-input {
    --input-size-s: 125px;
    --input-size-m: 250px;
    --input-size-l: 375px;

    appearance: none;
    display: inline-block;
    width: var(--input-size-m);
    height: 1.5rem;
    margin: 0.25rem 0;
    padding: 0.25rem 1em;
    vertical-align: top;
    line-height: 1rem;
    font-weight: normal;

    color: var(--color-secondary);
    background: var(--color-control);
    box-shadow: none;
    border: 1px solid var(--color-border);
    border-radius: 5px;
    transition: border-color .15s ease-in-out, box-shadow .15s ease-in-out;

    &:focus {
        outline: none;
        z-index: 1;
        transform: scale(1,1);
        border-color: var(--color-hover);
    }

    &:hover {
        &:not([disabled]),
        &:not([aria-disabled='true']) {
            border-color: var(--color-primary);
        }
    }

    &::placeholder {
      color: var(--color-border);
      font-size: var(--font-size-s);
      font-style: normal;
    }

    &[readonly] {
      background: var(--color-control);
      border-color: var(--color-border);
    }

    // remove "clear button" for IE
    &::-ms-clear {
      display: none;
    }

    &:-webkit-autofill,
    &:-webkit-autofill:hover,
    &:-webkit-autofill:focus {
      border-color: var(--color-border) !important;
      -webkit-text-fill-color: var(--color-secondary) !important;
      -webkit-box-shadow: 0 0 0 1000px var(--color-control) inset !important;
    }

    &[type='email'],
    &[type='password'],
    &[type='search'],
    &[type='text'],
    &[type='url'] {
        overflow: hidden;
        text-overflow: ellipsis;
        white-space: nowrap;
    }

    &[type='number'] {
        text-align: right;

        &::-webkit-inner-spin-button,
        &::-webkit-outer-spin-button {
            appereance: none;
            display: none;
        }
    }

    &[type='file'] {
        display: none;
    }

    &--small {
        width: var(--input-size-s);
    }

    &--medium {
        width: var(--input-size-m);
    }

    &--large {
        width: var(--input-size-l);
    }

    &--full {
        width: 100%;
    }

    &--auto {
        width: auto;
    }

    &--error,
    &--error:focus,
    &--error:hover {
        &:not([disabled]),
        &:not([aria-disabled='true']) {
            border-color: var(--color-error);
        }
    }
}
`;
