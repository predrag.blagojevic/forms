import React from "react";
import Styles from "$styles";
import UserContactForm from "../forms/UserContactForm";
import LoginForm from "../forms/LoginForm";
import RegisterForm from "../forms/RegisterForm";
import DemoForm from "../forms/DemoForm";

const App = () => (
    <>
        <Styles />
        <UserContactForm />
        <hr />
        <LoginForm />
        <hr />
        <RegisterForm />
        <hr />
        <DemoForm />
    </>
);

export default App;
