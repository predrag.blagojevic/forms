import React, {
    Children,
    cloneElement,
    isValidElement,
    useState,
    useEffect,
    useRef,
} from "react";
import { classNames, isPromise } from "$common/util";
import { Header, Body, Row, Footer, Messages } from "./sections";

const baseClass = "amb-form";

// Form state
const initialState = {
    isValid: true,
    isSubmitting: false,
    validation: {},
    message: "",
};

// Default form properties
const defaultProps = {
    layout: "vertical", // horizontal
    isValid: true,
    message: "",
    messages: {
        invalid: "Please check all the form fields and try again.",
        success: "Form successfully submitted.",
        error: "Error while submitting the form.",
    },
    onSubmit: () => {},
    onReset: () => {},
    onValidate: () => {},
};

/**
 * The Form component
 *
 * @param {Object} props - Component properties
 * @returns {Object} React component
 */
const Form = props => {
    const [state, setState] = useState(initialState);
    const setRefs = useRef(new Map()).current;

    const {
        title,
        header, // custom header
        layout, // "vertical" (default), "horizontal"
        width, // "full"
        reset, // add reset button
        submit, // add submit button
        footer, // custom footer
        className,
        children,
        isValid, // for manual form validation
        message, // for manual form validation
        messages, // default form messages
        onSubmit,
        onReset,
        onValidate, // triggers on validation change
        ...other
    } = props;

    const classes = classNames(
        baseClass,
        layout !== "vertical" && `${baseClass}--${layout}`,
        width && `${baseClass}--${width}`,
        className,
    );

    // Map props to state
    useEffect(() => setState(state => ({ ...state, isValid, message })), [
        props.isValid,
        props.message,
    ]);

    /**
     * Get the form data (the native way)
     *
     * @param {Object} form - HTML Form object
     * @returns {Object} Key / value pair
     */
    const getFormData = form => {
        const data = {};
        const formData = new FormData(form);

        for (const [key, value] of formData) {
            data[key] = value;
        }

        return data;
    };

    /**
     * Validate through all the form fields
     *
     * @param {Map} refs - Form element references
     * @returns {Boolean} True if all the fields are valid
     */
    const validateFields = refs => {
        const result = [];

        refs.forEach(ref => result.push(ref.validate && ref.validate()));

        return !Object.values(result).some(valid => !valid);
    };

    /**
     * Reset all the form fields
     *
     * @param {Map} refs - Form element references
     * @returns {void}
     */
    const resetFields = refs => refs.forEach(ref => ref.reset && ref.reset());

    /**
     * Reset form data
     *
     * @param {Object} event - Event object
     * @returns {void}
     */
    const handleReset = event => {
        event.preventDefault();
        resetFields(setRefs);
        setState(initialState);
    };

    /**
     * Handle form submission
     *
     * @param {Object} event - Event object
     * @returns {void}
     */
    const handleSubmit = event => {
        event.preventDefault();

        const isValid = validateFields(setRefs);
        if (!isValid) {
            return;
        }

        // disable the controls
        setState(state => ({ ...state, isSubmitting: true }));

        // try submitting the form by calling onSubmit
        const result = onSubmit(getFormData(event.target));

        // if onSubmit is a Promise, we expect it to return a message
        if (isPromise(result)) {
            result
                .then(message =>
                    setState(state => ({
                        ...state,
                        isValid: true,
                        message: message || messages.success,
                        isSubmitting: false,
                    })),
                )
                .catch(error =>
                    setState(state => {
                        console.error(messages.error);
                        return {
                            ...state,
                            isValid: false,
                            message: error || messages.error,
                            isSubmitting: false,
                        };
                    }),
                );
        }
        // otherwise we just display the default one
        else {
            setState(state => ({
                ...state,
                isValid: !state.message,
                message: state.message,
                isSubmitting: false,
            }));
        }
    };

    /**
     * Handle form validation
     *
     * @param {Object} validity - Validity object
     * @returns {void}
     */
    const handleValidation = fieldValidity =>
        setState(state => {
            // check for invalid fields within validation object
            const validation = { ...state.validation, ...fieldValidity };
            const isValid = !Object.values(validation).some(
                field => !field.isValid,
            );
            const message = isValid ? "" : state.message || messages.invalid;

            // call onValidate event handler
            onValidate(isValid, validation);

            // update form state
            return { ...state, isValid, validation, message };
        });

    /**
     * Inject onValidate, status and messages to the form fields
     *
     * @param {Array} children - React children
     * @returns {Array} React children
     */
    const withValidation = children =>
        Children.map(children, child => {
            // proceed only with our form components
            if (!child || !child.props || !isValidElement(child)) {
                return child;
            }

            // extract each field's status and messages
            const fieldName = child.props.name;
            const validity = state.validation[fieldName];
            const status = validity && !validity.isValid && "error";
            const messages = (validity && validity.messages) || null;

            // if it's more than one level deep, to it recursively
            if (child.props.children) {
                return cloneElement(child, {
                    children: withValidation(child.props.children),
                });
            }

            // assign refs to form fields
            const ref = el =>
                !el ? setRefs.delete(fieldName) : setRefs.set(fieldName, el);

            // return fieldTypes.includes(fieldName)
            return cloneElement(child, {
                messages: child.props.messages || messages || [],
                ref: fieldName ? ref : null,
                onValidate: handleValidation,
                status: child.props.status || status,
            });
        });

    return (
        <form
            noValidate
            className={classes}
            onSubmit={handleSubmit}
            onReset={handleReset}
            {...other}
        >
            {(title || header) && <Header title={title}>{header}</Header>}
            <Body>{withValidation(children)}</Body>
            {(footer || submit || reset) && (
                <Footer
                    submit={submit}
                    reset={reset}
                    isSubmitting={state.isSubmitting}
                >
                    {footer}
                </Footer>
            )}
            {state.message && (
                <Messages hasErrors={!state.isValid}>{state.message}</Messages>
            )}
        </form>
    );
};

Form.defaultProps = defaultProps;
Form.Header = Header;
Form.Body = Body;
Form.Row = Row;
Form.Footer = Footer;
Form.Messages = Messages;

export default Form;
