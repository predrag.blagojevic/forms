import React from "react";
import Button from "$components/Button";

const baseClass = "amb-form";

/**
 * Form footer section
 *
 */
const Footer = ({ submit, reset, isSubmitting, children }) => {
    return (
        <footer className={`${baseClass}__footer`}>
            {submit && (
                <Button type="primary" action="submit" busy={isSubmitting}>
                    {submit}
                </Button>
            )}
            {reset && <Button action="reset">{reset}</Button>}
            {children}
        </footer>
    );
};

export default Footer;
