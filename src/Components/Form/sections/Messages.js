import React from "react";
import { classNames } from "$common/util";

const baseClass = "amb-form";

/**
 * Form messages section
 *
 */
const Messages = ({ hasErrors, children }) => {
    const classes = classNames(
        `${baseClass}__messages`,
        hasErrors && `${baseClass}__messages--error`,
    );

    return <div className={classes}>{children}</div>;
};

export default Messages;
