import React from "react";

const baseClass = "amb-form";

/**
 * Form header section
 *
 */
const Header = ({ title, children }) => (
    <header className={`${baseClass}__header`}>
        {title && <h2 className={`${baseClass}__title`}>{title}</h2>}
        {children}
    </header>
);

export default Header;
