import React from "react";

const baseClass = "amb-form";

/**
 * Form body section
 *
 */
const Body = ({ children }) => {
    return <div className={`${baseClass}__body`}>{children}</div>;
};

export default Body;
