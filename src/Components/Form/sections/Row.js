import React from "react";

const baseClass = "amb-form";

/**
 * Form row wrapper
 *
 */
const Row = ({ children }) => {
    return <div className={`${baseClass}__row`}>{children}</div>;
};

export default Row;
