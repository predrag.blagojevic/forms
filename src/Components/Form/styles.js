import fieldStyles from "$components/Form/Field/styles.js";
import groupStyles from "$components/Form/Group/styles.js";

export default `
${fieldStyles}

${groupStyles}

.amb-form {
    display: inline-flex;
    flex-direction: column;
    padding: 1rem;
    border-radius: 10px;
    box-shadow: 2px 2px 8px 0px rgba(0,0,0,0.25);

    &--full {
        width: 100%;
    }

    &__header {
        width: 100%;
        margin: 0 0 1rem 0;
    }

    &__title {
        margin: 0;
        font-size: var(--font-size-xl);
    }

    &__body {
        display: flex;
        flex-direction: column;
        flex-wrap: wrap;
        align-items: flex-start;
    }

    &__row {
        display: flex;
        width: 100%;
    }

    &__row + &__row {
        margin-left: 1em;
    }

    &__row &-field + &-field {
        margin-left: 1em;
    }

    &-field {
        margin-bottom: 0.5rem;
    }

    &__footer {
        margin-top: 0.5rem;

        .amb-button + .amb-button {
            margin-left: 1em;
        }
    }

    &__messages {
        margin: 0.5rem 0 0 0;
        color: var(--color-hint);
        font-size: var(--font-size-s);

        &--error {
            color: var(--color-error);
        }
    }
}


.amb-form {
    --form-column-size: 125px;

    &--horizontal &-field {
        padding-top: 0;

        .amb-label {
            display: inline-grid;
            grid-template-columns: var(--form-column-size) auto;

            &__text {
                position: static;
                line-height: 2rem;;
                font-size: var(--font-size);
            }
        }
    }

    &--horizontal &-field--checkbox {
        .amb-label {
            display: inline-grid;
            grid-template-columns: var(--form-column-size) auto auto;
            align-items: center;

            &__text {
                grid-column: 1/2;
                grid-row: 1/2;
                position: static;
                line-height: 1.5rem;
                font-size: var(--font-size);
            }

            .amb-checkbox {
                grid-column: 2/3;
                grid-row: 1/2;
                margin-right: 0;
            }

            .amb-form-field {
                &__hint {
                    grid-column: 3/4;
                    grid-row: 1/2;
                    margin-left: 1em;
                }

                &__messages {
                    grid-column: 2/4;
                    grid-row: 2/3;
                    margin: 0;
                }
            }
        }
    }

    &--horizontal &-field &-group {
        display: inline-flex;
        align-items: center;

        &__hint {
            position: static;
            margin-left: 1em;
        }

        &__messages {
            position: absolute;
            bottom: 0;
            display: inline-flex;
        }
    }

    &--horizontal &-field__hint {
        width: 100%;
        margin-left: var(--form-column-size);
    }

    &--horizontal &-field .amb-fieldset {
        display: flex;
        // flex-direction: row;
        flex-wrap: wrap;
        position: relative;
        width: 100%;
        padding: 0 0 0 var(--form-column-size);
        border: none;

        &__legend {
            top: 0;
            font-size: var(--font-size);
            line-height: 1.5rem;
        }

        .amb-label {
            display: inline-flex;

            &__text {
                margin-right: 0;
                line-height: 1.5rem;
            }
        }
    }

    &--horizontal &__footer {
        padding-left: var(--form-column-size);
        text-align: left;
    }

    &--horizontal &__messages {
        // padding-left: var(--form-column-size);
    }
}
`;
