export default `
.amb-form-field {
    position: relative;
    display: inline-flex;
    padding-top: 0.5rem;

    &__hint {
        font-size: var(--font-size-xs);
        color: var(--color-hint);
    }

    &__messages {
        margin: 0;
        padding: 0;
        list-style: none;
        font-size: var(--font-size-xs);
        line-height: 0.5rem;
        color: var(--color-hint);
    }

    &__message {
        margin-right: 1ex;

        &::before {
            content: "\\2192";
            margin-right: 1ex;
        }
    }

    .amb-label {
        &__text {
            position: absolute;
            top: calc(-1 * var(--font-size));
        }
    }

    &--required {
        .amb-label__text::after {
            position: relative;
            top: -0.5ex;
            padding: 0 0.5ex;
            content: '*';
            font-size: var(--font-size-xs);
            color: var(--color-error);
        }
    }

    &--full {
        width: 100%;
    }

    &--inline {
        // height: 3rem;
        padding-top: 0;

        .amb-label {
            &__text {
                position: static;
                // line-height: 2rem;
            }
        }
    }

    &--inline + &--inline {
        // margin-left: 1em;
    }

    &--error &__messages {
        color: var(--color-error);
    }


    &--switch,
    &--checkbox {
        padding-top: 0;

        .amb-checkbox {
            order: 0;
        }

        .amb-label {
            flex-wrap: wrap;

            &__text {
                order: 1;
                position: static;
            }
        }

        .amb-form-field__hint {
            order: 2;
            width: 100%;
            margin-left: 1rem;
        }

        .amb-form-field__messages {
            order: 3;
            width: 100%;
            margin-left: 1rem;
        }
    }


    &--radio {
        flex-direction: column;
        margin-bottom: 1rem;
        padding-top: 0.5rem;

        .amb-radio {
            order: 0;
        }

        .amb-label {
            &__text {
                order: 1;
                position: static;
            }
        }

        .amb-fieldset__hint {
            order: 2;
        }
    }
}
`;
