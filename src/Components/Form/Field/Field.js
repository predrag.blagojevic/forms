import React, { forwardRef } from "react";
import { omit, classNames } from "$common/util";
import Label from "$components/Label";
import Input from "$components/Input";
import Textarea from "$components/Textarea";
import Select from "$components/Select";
import Radio, { RadioGroup } from "$components/Radio";
import Checkbox from "$components/Checkbox";
import Group from "$components/Form/Group";

const defaultProps = {
    type: "text",
    value: "",
};

const inputTypes = {
    text: Input,
    password: Input,
    number: Input,
    email: Input,
    select: Select,
    radio: Radio,
    checkbox: Checkbox,
    switch: Checkbox,
    textarea: Textarea,
};

const baseClass = "amb-form-field";

const Field = forwardRef((props, ref) => {
    const {
        type,
        id,
        disabled,
        hint,
        label,
        status,
        messages,
        inline,
        required,
        multiple,
        selected,
        width,
        className,
    } = props;

    const isRadio = type === "radio";
    const isTogglable = type === "checkbox" || type === "switch";

    const classes = classNames(
        baseClass,
        status && `${baseClass}--${status}`,
        width && `${baseClass}--${width}`,
        inline && `${baseClass}--inline`,
        required && `${baseClass}--required`,
        type === "checkbox" && `${baseClass}--checkbox`,
        type === "switch" && `${baseClass}--switch`,
        isRadio && `${baseClass}--radio`,
        className,
    );

    const FormField = inputTypes[type];
    const inputProps = omit(
        { ...props, ref, role: type === "switch" ? "switch" : null },
        ["hint", "messages", "inline"],
    );

    return (
        <div className={classes}>
            {!isTogglable && !isRadio && (
                <Label {...props} id={id} text={label} disabled={disabled}>
                    <Group
                        width={width}
                        input={<FormField {...inputProps} />}
                        hint={hint}
                        status={status}
                        messages={messages}
                    />
                </Label>
            )}

            {isTogglable && !isRadio && (
                <Label {...props} id={id} text={label} disabled={disabled}>
                    <FormField {...inputProps} />
                    {hint && (
                        <small className={`${baseClass}__hint`}>{hint}</small>
                    )}
                    {Array.isArray(messages) && messages.length > 0 && (
                        <ul className={`${baseClass}__messages`}>
                            {messages.map(message => (
                                <li
                                    key={message}
                                    className={`${baseClass}__message`}
                                >
                                    {message}
                                </li>
                            ))}
                        </ul>
                    )}
                </Label>
            )}

            {!isTogglable && isRadio && (
                <>
                    <RadioGroup
                        {...inputProps}
                        multiple={multiple}
                        selected={selected}
                        disabled={disabled}
                        inline={inline}
                    />
                    {hint && (
                        <small className={`${baseClass}__hint`}>{hint}</small>
                    )}
                    {Array.isArray(messages) && messages.length > 0 && (
                        <ul className={`${baseClass}__messages`}>
                            {messages.map(message => (
                                <li
                                    key={message}
                                    className={`${baseClass}__message`}
                                >
                                    {message}
                                </li>
                            ))}
                        </ul>
                    )}
                </>
            )}
        </div>
    );
});

Field.defaultProps = defaultProps;

export default Field;
