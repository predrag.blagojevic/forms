import React from "react";
import { classNames } from "$common/util";

const baseClass = "amb-form-group";

const Group = props => {
    const { input, hint, status, messages, inline, width, className } = props;

    const classes = classNames(
        baseClass,
        inline && `${baseClass}--inline`,
        status && `${baseClass}--${status}`,
        width && `${baseClass}--${width}`,
        className,
    );

    return (
        <div className={classes}>
            <div className={`${baseClass}__field`}>{input}</div>
            {hint && <small className={`${baseClass}__hint`}>{hint}</small>}
            {Array.isArray(messages) && messages.length > 0 && (
                <ul className={`${baseClass}__messages`}>
                    {messages.map(message => (
                        <li key={message} className={`${baseClass}__message`}>
                            {message}
                        </li>
                    ))}
                </ul>
            )}
        </div>
    );
};

export default Group;
