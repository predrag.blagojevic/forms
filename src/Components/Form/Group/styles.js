export default `
.amb-form-group {
    position: relative;
    padding-bottom: 0.5rem;

    &__field {

    }

    &__hint {
        position: absolute;
        top: calc(-1 * var(--font-size-s));
        right: 0;
        font-size: var(--font-size-xs);
        color: var(--color-hint);
    }

    &__messages {
        margin: 0;
        padding: 0;
        list-style: none;
        font-size: var(--font-size-xs);
        line-height: 0.5rem;
        color: var(--color-hint);
    }

    &__message {
        margin-right: 1ex;

        &::before {
            content: "\\2192";
            margin-right: 1ex;
        }
    }

    &--full,
    &--full &__field {
        width: 100%;
    }

    &--error &__messages {
        color: var(--color-error);
    }
}
`;
