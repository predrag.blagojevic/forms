export default `
.amb-checkbox,
.amb-checkbox[type='checkbox'] {
    --checkbox-size: 0.75rem;
    --checkbox-img-checked: url('data:image/svg+xml;utf8,<svg width="32" height="32" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><path fill="rgb(48,52,56)" d="M27 4L12 19l-7-7-5 5 12 12L32 9z"/></svg>');
    --checkbox-img-indeterminate: url('data:image/svg+xml;utf8,<svg width="32" height="32" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><path d="M0 13v6a1 1 0 001 1h30a1 1 0 001-1v-6a1 1 0 00-1-1H1a1 1 0 00-1 1z"/></svg>');

    appearance: none;
    flex: 0 0 auto;
    display: inline-block;
    width: var(--checkbox-size);
    height: var(--checkbox-size);
    margin: 0.125rem 0.25rem 0.125rem 0;
    padding: 0;
    cursor: pointer;
    vertical-align: top;
    line-height: 1rem;
    border: 1px solid var(--color-border);
    border-radius: 4px;
    background-color: var(--color-control);
    background-repeat: no-repeat;
    background-position: center center;

    &:focus {
        outline: none;
        z-index: 1;
        transform: scale(1,1);
        border-color: var(--color-hover);
    }

    &:hover {
        &:not([disabled]),
        &:not([aria-disabled='true']) {
            border-color: var(--color-primary);
        }
    }

    &:checked {
        border-color: var(--color-secondary);
        background-image: var(--checkbox-img-checked);
        background-size: 0.5rem;
    }

    &:indeterminate {
        background-image: var(--checkbox-img-indeterminate);
        background-size: 0.33rem;
    }

    & + .amb-label {
        margin-left: 1ex;
    }
}


.amb-checkbox--switch,
.amb-checkbox[role='switch'] {
    position: relative;
    display: inline-flex;
    width: 1.5rem;
    height: 1rem;
    margin: 0 0.25rem 0 0;
    padding: 0;
    border-radius: 0.5rem;

    &::before {
        position: absolute;
        top: 0;
        bottom: 0;
        content: "";
        display: inline-block;
        width: 100%;
        height: 100%;
        background: var(--color-control);
        border-radius: 0.5rem;
    }

    &::after {
        position: absolute;
        content: "";
        display: inline-block;
        width: 0.75rem;
        height: 0.75rem;
        margin: 2px;
        background: var(--color-hint);
        border-radius: 100%;
        transition: transform .15s ease-in-out;
    }

    &:checked {
        &::before {
            background: var(--color-control);
        }

        &::after {
            background: var(--color-secondary);
            transform: translateX(0.5rem);
        }
    }
}
`;
