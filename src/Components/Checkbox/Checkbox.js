import React, {
    useState,
    useEffect,
    useRef,
    forwardRef,
    useImperativeHandle,
} from "react";
import { classNames } from "$common/util";

const baseClass = "amb-checkbox";

const initialState = {
    checked: false,
    runValidation: true,
};

const defaultProps = {
    checked: false,
    indeterminate: false,
    onBlur: () => {},
    onChange: () => {},
};

const Checkbox = forwardRef((props, ref) => {
    const [state, setState] = useState(initialState);
    const elRef = useRef();
    const isFirstRun = useRef(true); // used to distinguish the first pass

    const {
        id,
        name,
        value,
        checked,
        indeterminate,
        role,
        validators,
        title,
        tabIndex,
        disabled,
        hidden,
        required,
        focus,
        autoFocus,
        className,
        onBlur,
        onChange,
        onValidate,
    } = props;

    const classes = classNames(
        baseClass,
        role === "switch" && `${baseClass}--switch`,
        className,
    );

    // make validate() function accessible via reference
    useImperativeHandle(ref, () => ({ validate, reset }));

    // didMount
    useEffect(() => {
        setState(state => ({ ...state, checked }));

        elRef.current.indeterminate = !!indeterminate;

        if (focus) {
            elRef.current.focus();
        }
    }, []);

    // set state.checked when props.checked changes
    useEffect(() => setState(state => ({ ...state, checked })), [
        props.checked,
    ]);

    // validate on state.checked change
    useEffect(() => {}, [state.checked]);
    useEffect(() => {
        if (isFirstRun.current) {
            isFirstRun.current = false;
            return;
        }

        if (state.runValidation) {
            validate();
        }

        setState(state => ({ ...state, runValidation: true }));
    }, [state.checked]);

    /**
     * Check field validity with provided validators
     *
     * @returns {Boolean} True if all the conditions are satisfied
     */
    const validate = () => {
        // the rest is relevant only if validators are passed to a field
        if (!validators) {
            return true;
        }

        // check field validity by running validators
        const messages = validators
            .reduce(
                (acc, validator) => [
                    ...acc,
                    validator(state.checked ? value : ""),
                ],
                [],
            )
            .filter(Boolean);

        const isValid = messages.length === 0;

        // notify parent Form about field's validity
        name &&
            onValidate({
                [name]: {
                    isValid,
                    messages,
                    value: state.checked ? value : "",
                },
            });

        return isValid;
    };

    /**
     * Reset field value
     *
     * @returns {Boolean} True if all the conditions are satisfied
     */
    const reset = () => setState({ ...initialState, runValidation: false });

    /**
     * Handle onBlur event
     *
     * @param {Object} e - Input event
     * @returns {void}
     */
    const handleBlur = ({ target: { checked } }) => {
        setState(state => ({ ...state, checked }));
        onBlur(checked ? value : "");
    };

    /**
     * Handle onChange event
     *
     * @param {Object} e - Input event
     * @returns {void}
     */
    const handleChange = ({ target: { checked } }) => {
        elRef.current.indeterminate = false;
        setState(state => ({ ...state, checked }));
        onChange(checked ? value : "");
    };

    return (
        <input
            type="checkbox"
            role={role}
            id={id}
            name={name}
            value={value}
            title={title}
            checked={state.checked}
            tabIndex={tabIndex}
            disabled={disabled}
            hidden={hidden}
            required={required}
            autoFocus={autoFocus}
            ref={elRef}
            onBlur={handleBlur}
            onChange={handleChange}
            className={classes}
        />
    );
});

Checkbox.defaultProps = defaultProps;

export default Checkbox;
