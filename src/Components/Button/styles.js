export default `
.amb-button {
    appearance: none;
    display: inline-flex;
    flex-direction: row;
    align-items: center;

    position: relative;
    height: 1.5rem;
    margin: 0.25rem 0;
    padding: 0.25rem 1em;
    line-height: 1rem;
    text-decoration: none;
    white-space: nowrap;
    font-weight: normal;
    white-space: nowrap;
    cursor: pointer;

    color: var(--color-primary);
    background: var(--color-control);
    border: 1px solid var(--color-primary);
    border-radius: 5px;

    &:hover,
    &:focus {
        z-index: 1;
        outline: none;
        text-decoration: none;

        &:not([disabled]),
        &:not([aria-disabled='true']) {
            background: rgba(11, 131, 99, 0.05);
            transition: all .15s ease-in-out 0s;
        }
    }

    &--primary {
        color: var(--color-window);
        background: var(--color-primary);

        &:hover,
        &:focus {
            &:not([disabled]),
            &:not([aria-disabled='true']) {
                color: var(--color-window);
                background: var(--color-hover);
                border-color: var(--color-hover);
            }
        }
    }

    .amb-loader {
        font-size: inherit;
        line-height: 1;
    }
}
`;
