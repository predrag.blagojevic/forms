import React from "react";
import Loader from "$components/Loader";
import { createAttributes, classNames } from "$common/util";

const baseClass = "amb-button";

const defaultProps = {
    action: "button",
    data: {},
    aria: {},
};

const dataAttributes = createAttributes("data");
const ariaAttributes = createAttributes("aria");

const Button = props => {
    const {
        id,
        action,
        busy,
        disabled,
        hidden,
        pressed,
        tabIndex,
        type,
        value,
        inputRef,
        data,
        aria,
        onClick,
        children,
        className,
    } = props;

    const classes = classNames(
        baseClass,
        type && `${baseClass}--${type}`,
        className,
    );

    return (
        <button
            id={id}
            type={action}
            aria-busy={busy}
            disabled={busy || disabled}
            hidden={hidden}
            aria-pressed={pressed}
            tabIndex={tabIndex}
            value={value}
            ref={inputRef}
            onClick={onClick}
            className={classes}
            {...dataAttributes(data)}
            {...ariaAttributes(aria)}
        >
            {busy && (
                <Loader
                    size="small"
                    spacing="right"
                    theme={type === "primary" ? "light" : null}
                />
            )}
            {children}
        </button>
    );
};

Button.defaultProps = defaultProps;

export default Button;
