export default `
.amb-loader {
    position: relative;
    display: inline-block;
    margin: 0;
    width: 2rem;
    font-size: 2rem;
    line-height: 2rem;
    text-indent: -9999em;

    &::after {
        position: absolute;
        display: inline-block;
        content: '';

        border: 2px solid rgba(15, 169, 128, 0.2);
        border-left: 2px solid var(--color-primary);
        border-radius: 50%;

        -webkit-transform: translateZ(0);
        -ms-transform: translateZ(0);
        transform: translateZ(0);
        -webkit-animation: loading 1.1s infinite linear;
        animation: loading 1.1s infinite linear;
    }

    &--small {
        width: var(--font-size);
        font-size: var(--font-size);
        line-height: var(--font-size);

        &::after {
            position: absolute;
            top: -1px;
            left: -1px;
            width: 0.75rem;
            height: 0.75rem;
        }
    }

    &--light::after {
        border-color: rgba(255, 255, 255, 0.2);
        border-left-color: var(--color-window);
    }

    &--left {
        margin-left: 1ex;
    }

    &--right {
        margin-right: 1ex;
    }

    @-webkit-keyframes loading {
        0% {
            -webkit-transform: rotate(0deg);
            transform: rotate(0deg);
        }
        100% {
            -webkit-transform: rotate(360deg);
            transform: rotate(360deg);
        }
    }

    @keyframes loading {
        0% {
            -webkit-transform: rotate(0deg);
            transform: rotate(0deg);
        }
        100% {
            -webkit-transform: rotate(360deg);
            transform: rotate(360deg);
        }
    }
}
`;
