import React from "react";
import { classNames } from "$common/util";

const baseClass = "amb-loader";

const Loader = props => {
    const {
        size,
        spacing,
        theme, // null ("dark"), "light",
        text = "Loading...",
        className,
    } = props;

    const classes = classNames(
        baseClass,
        size && `${baseClass}--${size}`,
        theme && `${baseClass}--${theme}`,
        spacing && `${baseClass}--${spacing}`,
        className,
    );

    return <span className={classes}>{text}</span>;
};

export default Loader;
