export default `
.amb-radio {
    --radio-size: 0.75rem;

    appearance: none;
    flex: 0 0 auto;
    display: inline-block;
    width: var(--radio-size);
    height: var(--radio-size);
    margin: 0.125rem 0.25rem 0.125rem 0;
    padding: 0;
    cursor: pointer;
    vertical-align: top;
    line-height: 1rem;
    background-color: var(--color-control);
    border: 1px solid var(--color-border);
    border-radius: 50%;

    &:focus {
        outline: none;
        z-index: 1;
        transform: scale(1,1);
        border-color: var(--color-hover);
    }

    &:hover {
        &:not([disabled]),
        &:not([aria-disabled='true']) {
            border-color: var(--color-primary);
        }
    }

    &:checked {
        border-color: var(--color-secondary);

        &::before {
            content: "";
            position: relative;
            top: 0.167rem;
            left: 0.167rem;
            display: inline-block;
            width: 0.333rem;
            height: 0.333rem;
            line-height: 0.333rem;
            overflow: hidden;
            background: var(--color-secondary);
            border-radius: 100%;
        }
    }

    & + .amb-label {
        margin-left: 1ex;
    }
}


.amb-fieldset {
    appearance: none;
    position: relative;
    display: flex;
    flex-direction: column;
    padding: 0.25rem 1em;
    border: 1px solid var(--color-border);
    border-radius: 5px;

    &__legend {
        position: absolute;
        top: -1rem;
        left: 0;
        margin: 0;
        line-height: 1rem;
        font-size: var(--font-size-s);
    }

    &__hint {
        margin-left: 1em;
        font-size: var(--font-size-xs);
    }

    &--radio {
        .amb-label--radio {
            align-items: center;
            line-height: 1.5rem;
        }
    }
}
`;
