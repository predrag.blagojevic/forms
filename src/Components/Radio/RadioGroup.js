import React, { useState, useEffect, forwardRef } from "react";
import { classNames, uuid } from "$common/util";
import Radio from "./Radio";
import Label from "$components/Label";

const baseClass = "amb-fieldset";

const initialState = {
    selected: "",
};

const defaultProps = {
    selected: "",
    inline: true,
    onChange: () => {},
};

const RadioGroup = forwardRef((props, ref) => {
    const [state, setState] = useState(initialState);

    const {
        name,
        value,
        label,
        disabled,
        selected,
        multiple,
        inline,
        children,
        className,
        onBlur,
        onFocus,
        onChange,
    } = props;

    const classes = classNames(
        baseClass,
        `${baseClass}--radio`,
        inline && `${baseClass}--inline`,
        className,
    );

    // didMount
    useEffect(() => {
        setState(state => ({ ...state, selected }));
    }, []);

    /**
     * Handle onChange event
     *
     * @param {Object} e - Input event
     * @returns {void}
     */
    const handleChange = value => {
        setState(state => ({ ...state, selected: value }));
        onChange(value);
    };

    /**
     * Create single radio input
     *
     * @param {Object} radioProps - Radio props
     * @returns {Object} React element
     */
    const createRadio = radioProps => {
        const {
            key,
            value,
            label,
            disabled,
            hint,
            selected,
            children,
        } = radioProps;

        const checked = !!selected;

        return (
            <Label {...props} disabled={disabled} text={label} key={key}>
                <Radio
                    name={name}
                    value={value}
                    selected={checked}
                    onChange={handleChange}
                    onBlur={onBlur}
                    onFocus={onFocus}
                />
                {children}
                {hint && <small className={`${baseClass}__hint`}>{hint}</small>}
            </Label>
        );
    };

    return multiple ? ( // multiple choices
        <fieldset className={classes}>
            <legend className={`${baseClass}__legend`}>{props.label}</legend>
            {multiple.map(({ value, label, hint, disabled }) =>
                createRadio({
                    key: uuid(),
                    selected: value === state.selected,
                    value: value,
                    label: label,
                    hint: hint,
                    disabled: disabled,
                }),
            )}
            {children}
        </fieldset>
    ) : (
        // single radio
        createRadio({
            key: uuid(),
            value,
            label,
            hint,
            selected,
            disabled,
            children,
        })
    );
});

RadioGroup.defaultProps = defaultProps;

export default RadioGroup;
