import React, { useRef, forwardRef } from "react";
import { classNames } from "$common/util";

const baseClass = "amb-radio";

const defaultProps = {
    selected: false,
    onBlur: () => {},
    onChange: () => {},
};

const Radio = forwardRef((props, ref) => {
    const elRef = useRef();

    const {
        id,
        name,
        value,
        title,
        tabIndex,
        disabled,
        hidden,
        required,
        autoFocus,
        selected,
        className,
        onBlur,
        onDragStart,
        onDrop,
        onFocus,
        onChange,
    } = props;

    const classes = classNames(baseClass, className);

    /**
     * Handle onChange event
     *
     * @param {Object} e - Input event
     * @returns {void}
     */
    const handleChange = () => onChange(value);

    return (
        <input
            type="radio"
            id={id}
            name={name}
            value={value}
            title={title}
            defaultChecked={selected}
            tabIndex={tabIndex}
            disabled={disabled}
            hidden={hidden}
            required={required}
            autoFocus={autoFocus}
            ref={elRef}
            className={classes}
            onChange={handleChange}
            onBlur={onBlur}
            onDragStart={onDragStart}
            onDrop={onDrop}
            onFocus={onFocus}
        />
    );
});

Radio.defaultProps = defaultProps;

export default Radio;
