import React from "react";
import { classNames } from "$common/util";

const baseClass = "amb-label";

const Label = props => {
    const {
        id,
        type,
        disabled,
        text,
        title,
        width,
        children,
        className,
    } = props;

    const isTogglable =
        type === "radio" || type === "checkbox" || type === "switch";

    const classes = classNames(
        baseClass,
        isTogglable && `${baseClass}--inline`,
        type === "radio" && `${baseClass}--radio`,
        type === "checkbox" && `${baseClass}--checkbox`,
        type === "switch" && `${baseClass}--switch`,
        width && `${baseClass}--${width}`,
        className,
    );

    return (
        <label
            className={classes}
            htmlFor={id}
            title={title}
            disabled={disabled}
        >
            {!isTogglable && text && (
                <span className={`${baseClass}__text`}>{text}</span>
            )}
            {children}
            {isTogglable && text && (
                <span className={`${baseClass}__text`}>{text}</span>
            )}
        </label>
    );
};

export default Label;
