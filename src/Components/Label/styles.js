export default `
.amb-label {
    position: relative;
    display: flex;
    flex-direction: row;
    align-items: flex-start;

    &__text {
        display: inline-flex;
        margin-right: 1ex;
        white-space: nowrap;
        font-size: var(--font-size-s);
    }

    &--full {
        width: 100%;
    }
}
`;
