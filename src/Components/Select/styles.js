export default `
.amb-select {
    --select-size-s: 125px;
    --select-size-m: 250px;
    --select-size-l: 375px;
    --select-img-arrow: url('data:image/svg+xml;utf8,<svg width="24" height="24" viewBox="0 0 24 24" fill="rgb(48,52,56)" xmlns="http://www.w3.org/2000/svg"><path d="M6.984 9.984h10.031l-5.016 5.016z"></path></svg>');

    appearance: none;
    display: inline-flex;
    align-items: center;
    min-width: var(--select-size-s);
    max-width: var(--select-size-l);
    min-height: 1.5rem;
    height: 1.5rem;
    margin: 0.25rem 0;
    padding: 0.25rem 1.25rem 0.25rem 1em;
    vertical-align: top;
    line-height: 1rem;
    font-weight: normal;

    color: var(--color-secondary);
    background: var(--color-control);
    box-shadow: none;
    border: 1px solid var(--color-border);
    border-radius: 5px;
    transition: border-color .15s ease-in-out, box-shadow .15s ease-in-out;
    background-image: var(--select-img-arrow);
    background-color: var(--color-control);
    background-repeat: no-repeat;
    background-position: right 4px center;
    background-size: 24px 24px;

    &:focus {
        outline: none;
        z-index: 1;
        transform: scale(1,1);
        border-color: var(--color-hover);
    }

    &:hover {
        &:not([disabled]),
        &:not([aria-disabled='true']) {
            border-color: var(--color-primary);
        }
    }

    &:not([multiple]) {
        &:hover,
        &:focus {
            &:not([disabled]):not([aria-disabled='true']) {
                border-color: var(--color-primary);
            }
        }
    }


    &:-webkit-autofill,
    &:-webkit-autofill:hover,
    &:-webkit-autofill:focus {
        border-color: var(--color-border) !important;
        -webkit-text-fill-color: var(--color-secondary) !important;
        -webkit-box-shadow: 0 0 0 1000px var(--color-control) inset !important;
    }

    // remove select arrow for IE
    &::-ms-expand {
        display: none;
    }

    &:not([icon]):empty::before {
        display: inline-block;
        content: '...';
    }

    & + .#{$namespace}-label {
        margin-left: 1ex;
    }

    &--fixed {
        width: var(--select-size-m);
    }
}


.amb-select[multiple] {
    flex-direction: column;
    height: auto;
    max-height: 7.5rem;
    padding: 0;
    vertical-align: top;
    background-image: none;

    > option {
        height: 1.5rem;
        padding: 0.333rem 1em;
    }
}
`;
