import React, {
    useState,
    useEffect,
    useRef,
    forwardRef,
    useImperativeHandle,
} from "react";
import { classNames, uuid } from "$common/util";

const baseClass = "amb-select";

const initialState = {
    value: "",
    runValidation: true,
};

const defaultProps = {
    multiple: false,
    value: "",
    selected: "",
    onChange: () => {},
    onKeyDown: () => {},
    onBlur: () => {},
    onValidate: () => {},
};

/**
 * Creates a single option element
 *
 * @param {Object} key
 * @param {String} value
 * @param {String} text
 * @returns {React.Element}
 */
const createOption = ({ key, value, text }) => (
    <option key={key || value || uuid()} value={value}>
        {text}
    </option>
);

/**
 * Creates an option group with option elements
 *
 * @param {String} label
 * @param {Object} options
 * @returns {React.Element}
 */
const createOptgroup = (label, options) => {
    const children = [];
    const keys = Object.keys(options);

    keys.forEach(key =>
        children.push(createOption({ key, value: key, text: options[key] })),
    );

    return (
        <optgroup key={uuid()} label={label}>
            {children}
        </optgroup>
    );
};

/**
 * Creates an array of option elements
 *
 * @param {Object|Array} options
 * @returns {Array<React.Element>}
 */
const createOptions = options => {
    const result = [];
    const keys = Object.keys(options);

    // simple array of [{ value, text }] options:
    if (Array.isArray(options)) {
        options.forEach(({ value, text }) =>
            result.push(createOption({ key: value, value, text })),
        );
    }
    // object literal: { "value": "text", ... } or { "group": { "value": "text", ... }, ... }
    else {
        keys.forEach(key => {
            const value = options[key];
            // optgroup:
            // - 'value' becomes a 'label' of the optgroup
            // - 'text' becomes the 'options' of the optgroup
            if (typeof value === "object") {
                result.push(createOptgroup(key, value));
            }
            // simple options: { value1: text1, value2: text2, ... valueN: textN }
            else {
                result.push(createOption({ key, value: key, text: value }));
            }

            return result;
        });
    }

    return result;
};

/**
 * Select functional component
 *
 * @param {PtopsT} props
 * @param {ContextT} context
 * @returns {React.Element}
 * @constructor
 */
const Select = forwardRef((props, ref) => {
    const [state, setState] = useState(
        props.multiple ? { ...initialState, value: [] } : initialState,
    );
    const elRef = useRef();
    const isFirstRun = useRef(true); // used to distinguish the first pass

    const {
        id,
        name,
        title,
        multiple,
        tabIndex,
        disabled,
        hidden,
        focus,
        readOnly,
        autoFocus,
        required,
        options,
        selected,
        width,
        className,
        validators,
        onBlur,
        onDragStart,
        onDrop,
        onFocus,
        onChange,
        onValidate,
    } = props;

    const classes = classNames(
        baseClass,
        width && `${baseClass}--${width}`,
        className,
    );

    // make validate() function accessible via reference
    useImperativeHandle(ref, () => ({ validate, reset }));

    // didMount
    useEffect(() => focus && elRef.current.focus(), []);

    // set state.value when props.value changes
    useEffect(
        () =>
            setState(state => ({
                ...state,
                value: multiple ? [selected] : selected,
            })),
        [selected],
    );

    // // validate on state.value change
    useEffect(() => {}, [state.value]);
    useEffect(() => {
        if (isFirstRun.current) {
            isFirstRun.current = false;
            return;
        }

        if (state.runValidation) {
            validate();
        }

        setState(state => ({ ...state, runValidation: true }));
    }, [state.value]);

    /**
     * Check field validity with provided validators
     *
     * @returns {Boolean} True if all the conditions are satisfied
     */
    const validate = () => {
        // the rest is relevant only if validators are passed to a field
        if (!validators) {
            return true;
        }

        // check field validity by running validators
        const messages = validators
            .reduce((acc, validator) => [...acc, validator(state.value)], [])
            .filter(Boolean);

        const isValid = messages.length === 0;

        // notify parent Form about field's validity
        name &&
            onValidate({
                [name]: {
                    isValid,
                    messages,
                    value: state.value,
                },
            });

        return isValid;
    };

    /**
     * Reset field value
     *
     * @returns {Boolean} True if all the conditions are satisfied
     */
    const reset = () => setState({ ...initialState, runValidation: false });

    /**
     * Handle onChange event
     *
     * @param {Object} e - Input event
     * @returns {void}
     */
    const handleChange = ({ target: { value } }) => {
        setState(state => ({
            ...state,
            value: multiple ? [value] : value,
        }));

        onChange(value);
    };

    /**
     * Handle onBlur event
     *
     * @param {Object} e - Input event
     * @returns {void}
     */
    const handleBlur = ({ target: { value } }) => {
        validate();
        onBlur(value);
    };

    return (
        <select
            multiple={multiple}
            id={id}
            name={name}
            title={title}
            tabIndex={tabIndex}
            disabled={disabled}
            hidden={hidden}
            readOnly={readOnly}
            required={required}
            autoFocus={autoFocus}
            value={state.value}
            ref={elRef}
            className={classes}
            onChange={handleChange}
            onBlur={handleBlur}
            onDragStart={onDragStart}
            onDrop={onDrop}
            onFocus={onFocus}
        >
            {createOptions(options, multiple)}
        </select>
    );
});

Select.defaultProps = defaultProps;

export default Select;
